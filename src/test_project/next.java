package test_project;
import java.lang.Exception;
import java.util.Scanner;
public class next {
	public static void main(String[]args) {
		Scanner scanner = new Scanner(System.in);
		scanner.useDelimiter("\n");
        System.out.print("Enter a number: ");
        try {
        	int ct=0,sum=0;
        	String number;
			while((number = scanner.next()) != null) {
				ct++;
				System.out.println(number);
				sum+=Integer.parseInt(number);
        	}
			float avg=sum/ct;
			System.out.println(avg);
		        		
        } catch (NumberFormatException e) {
            System.out.println("Invalid number: " + e.getMessage());
        }
	}

}
