package test_project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@FunctionalInterface
interface Calculator {
    int calculate(int a, int b);
    default void status() {};
}

public class lambda_function {

	    public static void main(String[] args) {
	        Calculator add = (a, b) -> { a*=2; return a + b;};
	        System.out.println(add.calculate(5, 3)); // Output: 8

	        Calculator subtract = (a, b) -> a - b;
	        System.out.println(subtract.calculate(5, 3)); // Output: 2
	        Runnable lambdaRunnable=()->{System.out.println("hi");
	        System.out.println("bye");
	        };
	        lambdaRunnable.run();
			List<Integer>list=new ArrayList<Integer>(Arrays.asList(1,7,9,2,3));
			int sum=list.stream().filter(n->n%2==0).mapToInt(Integer::intValue).sum();
			System.out.println(sum);

	    }

}

