package assignment1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

// Dictionary Map - Assignment 4
class DictionaryApp {
	private HashMap<String, String> dictHashMap = new HashMap<>();
	
	void addWord(String word, String definition) {
		dictHashMap.put(word, definition);
		System.out.println("Word added");
	}
	
	String getDefinition(String word) {
		
		if( dictHashMap.containsKey(word) )
			return word + ": " + dictHashMap.get(word);
		return "Word not found";
		
	}
	
//	suggestions retrieved if the word is a substring
	List<String> getSuggestions(String word) {
		List<String> suggestions = new ArrayList<>(); 
		for (String term : dictHashMap.keySet()) {
			if(term.contains(word))
				suggestions.add(term);
			
		}
		return suggestions;
		
	}

}

public class DictionaryAppTest{
	public static void main(String[] args) {
		DictionaryApp myApp = new DictionaryApp();
		System.out.println(myApp.getDefinition("lamb"));
		myApp.addWord("lamb", "lorem ipsum" );
		System.out.println(myApp.getDefinition("lamb"));
		myApp.addWord("rain", "lorem ipsum dolor" );
		System.out.println(myApp.getDefinition("lamb"));
		myApp.addWord("rainbow", "lorem ipsum dolor" );
		System.out.println(myApp.getSuggestions("rain"));


		
	}
}
