package assignment1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

//list filter - Assignment 6

class Product{
	private String name;
	private String category;
	public Product(String name, String category) {
		this.name = name;
		this.category = category;
	}
	
	public String getCategory() {
		return category;
	}
	
	public String getName() {
		return name;
	}
}
public class ListFilter {
	public static void main(String[] args) {
//		list of sample products
		ArrayList<Product> products = new ArrayList<>(Arrays.asList(
				new Product ("iPad","Electronics"),
				new Product("Chocolate", "Food"),
				new Product("Laptop", "Electronics")
				));
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Category: ");
		String category = scanner.nextLine();
		
//		using streams and filters
		ArrayList<String> resArrayList = products.stream()
				.filter(p -> p.getCategory().equalsIgnoreCase(category))
				.map( product -> product.getName())
				.collect(Collectors.toCollection(ArrayList::new));		
		
		System.out.println("Result: ");
		if(!resArrayList.isEmpty())
			System.out.println(resArrayList);
		else {
			System.out.println("No match");
		}
		scanner.close();
		
	}

}
