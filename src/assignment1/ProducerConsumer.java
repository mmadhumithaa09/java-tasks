package assignment1;

import java.util.ArrayList;

// produce consumer problem - Assignment 2
public class ProducerConsumer {
	private int size = 0;
	private ArrayList<Integer> buffer = new ArrayList<>();
	
	public ProducerConsumer(int size) {
		this.size = size;
		// TODO Auto-generated constructor stub
	}
	
//	producer adds items
	void producer() throws InterruptedException {
		int num = 1;
		while(true) {
//			ensures thread safety
			synchronized (this) {
//				check if buffer is full
				while(buffer.size()==size)
					wait();
				
				System.out.println("Producer: " + num);
				
//				update buffer with produced value
				buffer.add(num++);
				
//				notify consumer
				notify();
				
				Thread.sleep(1000);
				
			}
		}
	}
//	consumer retrieves items
	void consumer() throws InterruptedException {
		while(true) {
			synchronized (this) {
//				check if buffer is empty
				while(buffer.isEmpty())
					wait();
				
//				retrieve buffer value from top
				System.out.println("Consumer: " + buffer.get(0));
				buffer.remove(0);
				
//				notify producer
				notify();
				
				Thread.sleep(1000);
				
				
			}
		}
		
	}
	
	public static void main(String[] args) {
//		fixed buffer capacity
		ProducerConsumer producerConsumer = new ProducerConsumer(2);
//		initialise producer and consumer threads
		Runnable produce = () -> {
			try {
				producerConsumer.producer();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		};
		
		Runnable consume = () -> {
			try {
				producerConsumer.consumer();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		};
		
		Thread ThreadP = new Thread(produce);
		Thread ThreadC = new Thread(consume);
		
		ThreadP.start();
		ThreadC.start();
		
		
	}
}
