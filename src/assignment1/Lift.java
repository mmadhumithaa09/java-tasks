package assignment1;

import java.util.Collections;
import java.util.PriorityQueue;

// lift system - Assignment 5
class LiftSystem{
//	sorted levels for going up (min-heap) and reverse sorted while going down (max-heap)
	private static PriorityQueue<Integer> queueUp = new PriorityQueue<>();
	private static PriorityQueue<Integer> queueDown = new PriorityQueue<>(Collections.reverseOrder());
//	total # of floors
	private static int levels = 0;
//	default current level
	private static int currentLevel = 0;
//	indicates if the initial config is to ascend/descend
	static boolean isUp = true;
	
//	configure floors in the lift
	public LiftSystem(int levels) {
		LiftSystem.levels = levels;
		System.out.println("Current Level " + currentLevel);
	}
	
//	return the current level
	public int getLevel() {
		return currentLevel;
	}
	
//	ascent
	public static void goUp(int floor) throws InterruptedException {
		if(currentLevel + floor > levels-1) {
			System.out.println("Invalid Floor");
			return;
		}
		System.out.println("Up " + floor + " floors");
		Thread.sleep(2000);	
//		update current floor		
		currentLevel += floor;
	}
	
//	descent
	public static void goDown(int floor) throws InterruptedException {
		if(currentLevel - floor < 0) {
			System.out.println("Invalid");
			return;
		}
		System.out.println("Down " + floor + " floors");
		Thread.sleep(1000);
		currentLevel -= floor;
	}
	
//	update the priority queue with input floors
	void updateQueue(int floor) {
		if(floor > currentLevel)
			queueUp.offer(floor);
		else if(floor < currentLevel)
			queueDown.offer(floor);
//		same as current floor not required
	}
	
//	simulate the lift movement
	void motion() throws InterruptedException {
//		lift keeps moving until both queues are empty
		while(!queueDown.isEmpty() || !queueUp.isEmpty()) {
//			lift ascent
			if(isUp) {
				while(!queueUp.isEmpty()) {
	//				remove from priority queue after moving
					goUp(queueUp.peek() - currentLevel);
					queueUp.poll();
					System.out.println("Current Level " + currentLevel);
				}
			}
	//		lift descent
			else {
				while(!queueDown.isEmpty()) {
	//				remove from priority queue after moving
					goDown(currentLevel - queueDown.peek());
					queueDown.poll();
					System.out.println("Current Level " + currentLevel);
				}
			}
			
			isUp = !isUp;
		}
	}
	
}

public class Lift {
	
	public static void main(String[] args) throws InterruptedException {
		
//		sample lift operation
		LiftSystem newLift = new LiftSystem(10);
		
		newLift.updateQueue(9);
		newLift.updateQueue(2);
		
//		lift moves with the given input user levels
		newLift.motion();
		
		newLift.updateQueue(1);
		newLift.updateQueue(3);
		newLift.motion();
		
		System.out.println("Lift stops");
		
		
	}

}
