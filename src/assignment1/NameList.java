package assignment1;

import java.util.ArrayList;
//import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

// Name list mapping - Assignment 7
class User{
	private String name;
	@SuppressWarnings("unused")
	private int id;
	public User(String name) {
		this.name = name;
		this.id = new Random().nextInt();
	}
	
	public String getName() {
		return name;
	}
}
public class NameList {
	
	public static void main(String[] args) {
//		sample users
		ArrayList<User> users = new ArrayList<>();
		users.add(new User("Alice"));
		users.add(new User("Bob"));
		users.add(new User("Cassie"));
		
//		List<String> names = users.stream().
//				map(user -> user.getName()).
//				collect(Collectors.toList());
		
//		using streams and mapping required names
		ArrayList<String> names = users.stream().
				map(user -> user.getName()).
				collect(Collectors.toCollection(ArrayList::new));
		System.out.println("User names: " + names);

		
	}

}
