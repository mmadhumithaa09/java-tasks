package assignment1;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

// Thread Pool with Executor Service - Assignment 1
// task for printing thread 
class Task implements Runnable{
	private String name;
	public Task(String name) {
		this.name = name;
		// TODO Auto-generated constructor stub
	}
	@Override
	public void run() {
		String msgString= "Executing " + name + " by Thread : " + Thread.currentThread().getName();
		System.out.println(msgString);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

public class ThreadPool {
	public static void main(String[] args) {
//		fixed sized thread pool
		ExecutorService executorService = Executors.newFixedThreadPool(5);
		System.out.println("Thread Pool Created");
//		submit all tasks to the executor
		for(int i=0;i<10;) {
			Task newTask = new Task("Task"+ ++i);
			executorService.submit(newTask);		
		}
//		shutdown the executor
		executorService.shutdown();
		try {
//			wait for 10s until unfinished tasks are completed
			if(executorService.awaitTermination(10, TimeUnit.SECONDS))
				System.out.println("Thread pool terminated");
			else {
				System.out.println("Thread pool not terminated");
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			executorService.shutdown();
			e.printStackTrace();
		}
			
	}

}
