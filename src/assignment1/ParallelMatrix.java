package assignment1;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

// parallel matrix multiplication - Assignment 3
class Matrix implements Runnable{
	private int[][] m1;
	private int[][] m2;
	private int[][] res;
//	row and 
	int r,n,p;
//	matrices and dimensions and input row taken as parameters 
	public Matrix(int [][] m1, int [][] m2, int [][] res, int r, int n, int p) {
		this.m1 = m1;
		this.m2= m2;
		this.res = res;
		this.r = r;
		this.n = n;
		this.p = p;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
//		modify only the rth row value
		System.out.println("Executing row" + r + " by Thread : " + Thread.currentThread().getName());

		for(int i=0;i<p;++i) {
			res[r][i] = 0;
			for(int j=0;j<n;++j)
//				each row vector multiplied with column vector 
				res[r][i] +=  m1[r][j]*m2[j][i];
		}
	}
}

public class ParallelMatrix {
//	generate random matrix with given size
	static int[][] randomMat(int r, int c){
		Random ran = new Random();
		int [][] mat = new int[r][c];
		for(int i=0;i<r;i++) {
			for(int j=0;j<c;j++)
				mat[i][j] = (int) ran.nextInt(10);
		}
		return mat;
	}
	
	static void print(int [][] m, int r, int c) {
		for(int i=0;i<r;i++) {
			for(int j=0;j<c;j++)
				System.out.print(m[i][j] + " ");
			System.out.println();
		}
	}
	
	static void multiply(int [][] m1, int [][] m2, int m, int n, int p, int [][] res) {
//		creating a thread pool of fixed size
		ExecutorService executorService = Executors.newFixedThreadPool(p);
//		submit each row as a task
		for(int i=0;i<m;++i) {
			Matrix matrix = new Matrix(m1, m2, res, i, n, p);
			executorService.execute(matrix);
						
		}
		
//		shutdown the executor
		executorService.shutdown();
		
		try {
//			wait for all tasks to be finished
			if(executorService.awaitTermination(10, TimeUnit.SECONDS))
				System.out.println("Thread pool terminated");
			else {
				System.out.println("Thread pool not terminated");
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			executorService.shutdown();
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter Matrix sizes: \n " + "m = ");
		int m = scanner.nextInt();
		System.out.println("n = ");
		int n = scanner.nextInt();
		System.out.println("p = ");
		int p = scanner.nextInt();
		scanner.close();
		
		int [][] m1 = randomMat(m, n);
		int [][] m2 = randomMat(n, p);
		int [][] res = new int[m][p];
		
		print(m1, m, n);
		print(m2, n, p);

//		measure execution time
		long start = System.currentTimeMillis();
		multiply(m1, m2, m, n, p, res);
		long end = System.currentTimeMillis();
		
//		print product matrix
		print(res, m, p);
		
		System.out.println("Execution Time : " + (end-start) + "ms");
		
	}

}
