package my_tasks;


public class task5 {		
		public static void main(String[] args) {
			Runnable r1 = new Runnable() {
				public void run() {
					try {
						for (int i = 0; i <= 100; i+=2) {
							System.out.println(i);				
							Thread.sleep(500);
							}
					} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
					}
					
				};
			
			Runnable lambdaRunnable = () -> {
				try {
					for (int i = 1; i <= 100; i+=2) {
						System.out.println(i);
						Thread.sleep(500);
						
					}
				}
					catch (InterruptedException e) {
						// TODO: handle exception
						e.printStackTrace();
						
					}
				};
			
			Thread t1 = new Thread(r1);
			Thread t2 = new Thread(lambdaRunnable);
					
			t1.start();
			t2.start();
			
		}

}
