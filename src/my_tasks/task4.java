package my_tasks;

import java.util.Scanner;

//vowels count - FUNCTIONAL PROGRAMMING
public class task4 {
	public static void main(String[] args) {
		Scanner scanner=new Scanner(System.in);
		String string= scanner.nextLine();
//		return a stream of chars and perform map reduce
		int ct=string.toLowerCase().chars()
				.filter(n-> (n=='a' || n=='e' || n=='i' || n=='o' || n=='u'))
				.map(s->1)
				.reduce(0, Integer::sum);
		System.out.println(ct);
	}

}
