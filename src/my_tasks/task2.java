package my_tasks;
//stack using array

import java.util.ArrayList;
import java.util.Scanner;

public class task2 {
	private ArrayList<Integer>list=new ArrayList<>();
	public static Scanner scanner=new Scanner(System.in);
	public void push (Integer x) {
		list.add(x);
	}
	public void pop () {
		if(list.isEmpty())
			return;
		list.remove(list.size()-1);		
	}
	public int top() {
		if(!list.isEmpty())
			return list.get(list.size()-1);
		return -1;
	}
	public boolean isempty() {
		return list.isEmpty();
	}
	public static void main(String[] args) {
//		sample		
		task2 objTask2=new task2();
		objTask2.pop();
		objTask2.push(3);
		System.out.println(objTask2.top());
		objTask2.pop();
		objTask2.pop();
		System.out.println(objTask2.top());
		
	}

}
