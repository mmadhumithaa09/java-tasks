package my_tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

//sum evens
public class task3 {
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		Scanner scanner = new Scanner(System.in);
		try {
			while (scanner.hasNextInt()) {
				list.add(scanner.nextInt());
			}
			float sum = list.stream()					
					.filter(n -> n %2==0)
					.reduce(0,Integer::sum);
			System.out.println(sum);
//			List<Integer> sum = list.stream()
//					.map(s->Integer.valueOf(s*s))
//					.filter(n -> n > 78).collect(Collectors.toList());
//			System.out.println(sum);
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Invalid"+e.getMessage());
		}
	}

}
