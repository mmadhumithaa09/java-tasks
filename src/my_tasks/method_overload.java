package my_tasks;

class method_overload{  
	  void sum(int a,int b){System.out.println(a+b);}  
	  void sum(int a,int b,int c){System.out.println(a+b+c);}  
	  
	  public static void main(String args[]){  
	  method_overload obj=new method_overload();  
	  obj.sum(20,'a');//now second int literal will be promoted to long  
	  obj.sum(20,20,20);  
	  
	  }  
	} 
